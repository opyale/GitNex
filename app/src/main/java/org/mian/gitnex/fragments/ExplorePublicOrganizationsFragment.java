package org.mian.gitnex.fragments;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import org.gitnex.tea4j.models.Organization;
import org.mian.gitnex.R;
import org.mian.gitnex.adapters.PublicOrganizationsAdapter;
import org.mian.gitnex.clients.RetrofitClient;
import org.mian.gitnex.databinding.FragmentOrganizationsBinding;
import org.mian.gitnex.helpers.Authorization;
import org.mian.gitnex.helpers.Constants;
import org.mian.gitnex.helpers.SnackBar;
import org.mian.gitnex.helpers.TinyDB;
import org.mian.gitnex.helpers.Version;
import java.util.ArrayList;
import java.util.List;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Author M M Arif
 */

public class ExplorePublicOrganizationsFragment extends Fragment {

	private FragmentOrganizationsBinding fragmentPublicOrgBinding;
	private List<Organization> organizationsList;
	private PublicOrganizationsAdapter adapter;
	private Context context;
	private int pageSize;
	private final String TAG = Constants.publicOrganizations;
	private int resultLimit = Constants.resultLimitOldGiteaInstances;

	@Nullable
	@Override
	public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

		fragmentPublicOrgBinding = FragmentOrganizationsBinding.inflate(inflater, container, false);
		context = getContext();

		TinyDB tinyDb = TinyDB.getInstance(getContext());
		final String loginUid = tinyDb.getString("loginUid");
		final String instanceToken = "token " + tinyDb.getString(loginUid + "-token");

		// if gitea is 1.12 or higher use the new limit
		if(new Version(tinyDb.getString("giteaVersion")).higherOrEqual("1.12.0")) {
			resultLimit = Constants.resultLimitNewGiteaInstances;
		}

		fragmentPublicOrgBinding.addNewOrganization.setVisibility(View.GONE);
		organizationsList = new ArrayList<>();

		fragmentPublicOrgBinding.pullToRefresh.setOnRefreshListener(() -> new Handler(Looper.getMainLooper()).postDelayed(() -> {
			fragmentPublicOrgBinding.pullToRefresh.setRefreshing(false);
			loadInitial(instanceToken, resultLimit);
			adapter.notifyDataChanged();
		}, 200));

		adapter = new PublicOrganizationsAdapter(getContext(), organizationsList);
		adapter.setLoadMoreListener(() -> fragmentPublicOrgBinding.recyclerView.post(() -> {
			if(organizationsList.size() == resultLimit || pageSize == resultLimit) {
				int page = (organizationsList.size() + resultLimit) / resultLimit;
				loadMore(Authorization.get(getContext()), page, resultLimit);
			}
		}));

		DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(fragmentPublicOrgBinding.recyclerView.getContext(), DividerItemDecoration.VERTICAL);
		fragmentPublicOrgBinding.recyclerView.setHasFixedSize(true);
		fragmentPublicOrgBinding.recyclerView.addItemDecoration(dividerItemDecoration);
		fragmentPublicOrgBinding.recyclerView.setLayoutManager(new LinearLayoutManager(context));
		fragmentPublicOrgBinding.recyclerView.setAdapter(adapter);

		loadInitial(Authorization.get(getContext()), resultLimit);

		return fragmentPublicOrgBinding.getRoot();
	}

	private void loadInitial(String token, int resultLimit) {

		Call<List<Organization>> call = RetrofitClient
			.getApiInterface(context).getAllOrgs(token, Constants.publicOrganizationsPageInit, resultLimit);
		call.enqueue(new Callback<List<Organization>>() {
			@Override
			public void onResponse(@NonNull Call<List<Organization>> call, @NonNull Response<List<Organization>> response) {
				if(response.isSuccessful()) {
					if(response.body() != null && response.body().size() > 0) {
						organizationsList.clear();
						organizationsList.addAll(response.body());
						adapter.notifyDataChanged();
						fragmentPublicOrgBinding.noDataOrg.setVisibility(View.GONE);
					}
					else {
						organizationsList.clear();
						adapter.notifyDataChanged();
						fragmentPublicOrgBinding.noDataOrg.setVisibility(View.VISIBLE);
					}
					fragmentPublicOrgBinding.progressBar.setVisibility(View.GONE);
				}
				else if(response.code() == 404) {
					fragmentPublicOrgBinding.noDataOrg.setVisibility(View.VISIBLE);
					fragmentPublicOrgBinding.progressBar.setVisibility(View.GONE);
				}
				else {
					Log.e(TAG, String.valueOf(response.code()));
				}
			}

			@Override
			public void onFailure(@NonNull Call<List<Organization>> call, @NonNull Throwable t) {
				Log.e(TAG, t.toString());
			}
		});
	}

	private void loadMore(String token, int page, int resultLimit) {

		fragmentPublicOrgBinding.progressLoadMore.setVisibility(View.VISIBLE);
		Call<List<Organization>> call = RetrofitClient.getApiInterface(context).getAllOrgs(token, page, resultLimit);
		call.enqueue(new Callback<List<Organization>>() {
			@Override
			public void onResponse(@NonNull Call<List<Organization>> call, @NonNull Response<List<Organization>> response) {
				if(response.isSuccessful()) {
					List<Organization> result = response.body();
					if(result != null) {
						if(result.size() > 0) {
							pageSize = result.size();
							organizationsList.addAll(result);
						}
						else {
							SnackBar.info(context, fragmentPublicOrgBinding.getRoot(), getString(R.string.noMoreData));
							adapter.setMoreDataAvailable(false);
						}
					}
					adapter.notifyDataChanged();
					fragmentPublicOrgBinding.progressLoadMore.setVisibility(View.GONE);
				}
				else {
					Log.e(TAG, String.valueOf(response.code()));
				}
			}

			@Override
			public void onFailure(@NonNull Call<List<Organization>> call, @NonNull Throwable t) {
				Log.e(TAG, t.toString());
			}
		});
	}
}
